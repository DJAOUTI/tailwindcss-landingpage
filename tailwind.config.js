/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    extend: {
      colors:{
        DarkGreen: 'hsl(158, 90%, 12%)',// hexa : #033925
        MiddleGreen: 'hsl(164, 83%, 19%)',// hexa : #085842
        LightGreen: 'hsl(149, 57%, 35%)',// hexa : #278E59
        DarkYellow: 'hsl(49, 71%, 40%)',// hexa : #AF941E
        GoldYellow: 'hsl(43, 83%, 49%)',// hexa : #E4AB15
        MiddleYellow: 'hsl(49, 56%, 54%)',// hexa : #CBB247
        LightYellow: 'hsl(47, 99%, 59%)',// hexa : #FED131
        LightRed: 'hsl(355, 86%, 55%)',// hexa : #EF2C3D
        MiddleRed: 'hsl(357, 96%, 26%)',// hexa : #830309
        DarkRed: 'hsl(358, 96%, 20%)',// hexa : #630205
        WhiteColor: 'hsl(0, 0%, 100%)', // hexa : #FFFFFF
        LightGrey: 'hsl(0, 0%, 85%)', // hexa :  #D9D9D9
      }
    },
  },
  plugins: [],
}

